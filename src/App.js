import React,{useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
import {Container} from 'react-bootstrap'

import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'

import NavBar from './components/NavBar'
import Register from './pages/register'
import Login from './pages/login'
import Categories from './pages/categories'
import Home from './pages/home'
import Income from './pages/income'
import Expense from './pages/expense'
import NotFound from './pages/notFound'
import Landing from './pages/landing'

import {UserProvider} from './userContext'


function App(){
  const [user,setUser] = useState({
    email: localStorage.getItem('email'),
    isAdmin: JSON.parse(localStorage.getItem('isAdmin'))
  })
// console.log(user)

function unsetUser(){
  localStorage.clear()
}

  return(

      <>
        <UserProvider value={{user,setUser,unsetUser}}>
          <Router>
          <NavBar />
               <Container>
                  <Switch>
                    {user.email
                      ? <>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/register" component={Home} />
                        <Route exact path="/login" component={Home} />
                        <Route exact path="/categories" component={Categories} />
                        <Route exact path="/income" component={Income} />
                        <Route exact path="/expense" component={Expense} />
                        {/*<Route component={NotFound} />*/}
                        </>
                      : <>
                        <Route exact path="/" component={Landing}/>
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/categories" component={Login} />
                        <Route exact path="/income" component={Login} />
                        <Route exact path="/expense" component={Login} />
                        {/*<Route component={NotFound} />*/}
                        </>
                    }
                  </Switch>
               </Container>
          </Router>
        </UserProvider>
      </>

    )
}

export default App;