import React,{useContext, useState, useEffect} from 'react'

import UserContext from '../userContext'
import Container from 'react-bootstrap/Container'


import { Redirect } from 'react-router-dom'

import '../App.css'

function Dashboard({totalBalance}) {
    const {user} = useContext(UserContext)

    
    const [allEntries, setAllEntries] = useState([])
    const [incomeEntries, setIncomeEntries] = useState([]);
    const [expenseEntries, setExpenseEntries] = useState([]);
    const [incomeAmountArray, setIncomeAmountArray] = useState();
    const [expenseAmountArray, setExpenseAmountArray] = useState([])
    const [update, setUpdate] = useState("")

 
     useEffect(()=>{
        let token = localStorage.getItem('token')
        fetch('https://thawing-refuge-32176.herokuapp.com/api/entries',{

        method: 'GET',   
        headers: {

                Authorization: `Bearer ${localStorage.getItem('token')}`

                    }
       }).then(res=>res.json())
        .then(data=>{
            // console.log(data)
            setAllEntries(data)

            let entriesTemp = data

            let incomeList= entriesTemp.filter(income=>{
                return income.type === "Income"
            })

            let expenseList = entriesTemp.filter(expense=>{
                return expense.type === "Expense"
            })
            // console.log(incomeList)
            // console.log(expenseList)
            setIncomeEntries(incomeList)
            setExpenseEntries(expenseList)

          
      
        })          

    }, [update])    

        const incomeAmount = incomeEntries.map(amount => amount.amount)
        const incomeTotal = incomeAmount.reduce((acc, item)=>(acc += item), 0).toFixed(2);

         const expenseAmount = expenseEntries.map(amount => amount.amount)
         const expenseTotal = expenseAmount.reduce((acc, item)=>(acc += item), 0).toFixed(2);

         const balance = incomeTotal - expenseTotal

         // console.log(balance)
         const formatter = new Intl.NumberFormat('en-US', {
            stlye: 'currency',
            currency: 'PHP',
            minimumFractionDigits: 2
         })

 
         return (
         	user.email
         	?
         		<>
                    <Container className="gray home">
         			<h1> Summary </h1>
         			<table className="table">
         				<tbody>
         					<tr>
         						<th>
         							<div>Income</div>
         							<small className="font-italic">
         								<a href="/income">View All</a>
         							</small>
         						</th>
         						<td>
         							<span>+₱{formatter.format(incomeTotal)}</span>
         						</td>
         					</tr>
         					<tr>
         						<th>
         							<div>Expenses</div>
         							<small className="font-italic">
         								<a href="/expense">View All</a>
         							</small>
         						</th>
         						<td>
         							<span>-₱{formatter.format(expenseTotal)}</span>
         						</td>
         					</tr>
         					<tr>
         						<th>
         							<div>Balance</div>
         						</th>
         						<td>
         							₱<span>{formatter.format(balance)}</span>
         						</td>
         					</tr>
         				</tbody>
         			</table>
                    </Container>
         		</>
         		:
         		<Redirect to ='/login'/>
         	)
}

export default Dashboard