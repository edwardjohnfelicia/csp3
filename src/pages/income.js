import React, {useState, useEffect, useContext} from 'react'

import AddIncomeEntry from '../components/AddIncomeEntry'

import {Form, Button, Table} from 'react-bootstrap'

import UserContext from '../userContext'

import { Redirect } from 'react-router-dom'
import Container from 'react-bootstrap/Container'


import '../App.css'


function Income(){

    const {user} = useContext(UserContext)

    
    const [allEntries, setAllEntries] = useState([])
    const [incomeEntries, setIncomeEntries] = useState([])

    const [update, setUpdate] = useState("")

 
    
     useEffect(()=>{

        let token = localStorage.getItem('token')

        fetch('https://thawing-refuge-32176.herokuapp.com/api/entries',{

        method: 'GET',   
        headers: {

                Authorization: `Bearer ${localStorage.getItem('token')}`

                    }
       }).then(res=>res.json())
        .then(data=>{
            console.log(data)
            setAllEntries(data)

            let  entriesTemp = data

            let incomeEntries = entriesTemp.filter(entry=>{

                return entry.type === "Income"
            })
             console.log(incomeEntries)
             setIncomeEntries(incomeEntries)
        })


    }, [update])

            const formatter = new Intl.NumberFormat('en-US', {
            stlye: 'currency',
            currency: 'PHP',
            minimumFractionDigits: 2
            })
             let incomeRows = incomeEntries.map(income=>{
                const sign = income.type === "Income" ? '+' : '-';
                return (

                    <tr key={income._id}>
                        <td>{income.category}</td>
                        <td className="plus">{sign}₱{formatter.format(income.amount)}</td>
                    </tr>


                    )
             })
            

    return (
       <>
       <Container className="gray entry">
            <h1>Income</h1>
           <div> 
             <AddIncomeEntry />
           </div> 
           <div>
              <h2>History</h2>
              <Table className="history-table">
                  <thead>
                      <tr>
                          <th>Category</th>
                          <th>Amount</th>
                      </tr>
                  </thead>
                  <tbody>
                      {incomeRows}
                  </tbody>
              </Table>
           </div>
           </Container>
        </>
        

    )
}

export default Income