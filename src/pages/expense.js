import React, {useState, useEffect, useContext} from 'react'

import AddExpenseEntry from '../components/AddExpenseEntry'

import {Form, Button, Table} from 'react-bootstrap'

import UserContext from '../userContext'

import { Redirect } from 'react-router-dom'
import Container from 'react-bootstrap/Container'

 
import '../App.css'

function Expense(){
 const {user} = useContext (UserContext)

 
 const [allEntries, setAllEntries] = useState([])
 const [expenseEntries, setExpenseEntries] = useState([])

 const [update, setUpdate] = useState("")

 
  useEffect(()=>{
     let token = localStorage.getItem('token')

     fetch('https://thawing-refuge-32176.herokuapp.com/api/entries',{

     method: 'GET',   
     headers: {

             Authorization: `Bearer ${localStorage.getItem('token')}`

                 }
    }).then(res=>res.json())
     .then(data=>{
         console.log(data)
         setAllEntries(data)

         let  entriesTemp = data

         let expenseEntries = entriesTemp.filter(entry=>{

             return entry.type === "Expense"
         })
          
          setExpenseEntries(expenseEntries)
     })



 }, [update])    
            const formatter = new Intl.NumberFormat('en-US', {
            stlye: 'currency',
            currency: 'PHP',
            minimumFractionDigits: 2
            })
        
            let expenseRows = expenseEntries.map(expense=>{
            const sign = expense.type === "Expense" ? '-' : '+';
               
             return (

                 <tr key={expense._id}>
                     <td>{expense.category}</td>
                     <td className="minus">{sign}₱{formatter.format(expense.amount)}</td>
                 </tr>


                 )
        
          })



          

    return (
        
        user.email 
        ?
        <>
        <Container className="gray entry">
        <h1>Expense</h1>
           <div>
            <AddExpenseEntry />
           </div>  
            <div>
              <h2>History</h2>
              <Table className="history-table">
                  <thead>
                      <tr>
                          <th>Category</th>
                          <th>Amount</th>
                      </tr>
                  </thead>
                  <tbody>
                      {expenseRows}
                  </tbody>
              </Table>
           </div>
           </Container>
        </>
        :
        <Redirect to='/'/>
    )
}

export default Expense