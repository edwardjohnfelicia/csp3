import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container'
import { useHistory } from 'react-router-dom';
  
import React from 'react';

export default function NotFound() {

	const history = useHistory()

	const routeChange = () =>{ 
	  let path = `/`; 
	  history.push(path);
	}

  return (
    <Jumbotron fluid>
      <Container>
        <h1>Page not Found!</h1>
        <Button variant="primary" onClick={routeChange}>
         Back to Home
        </Button>
      </Container>
    </Jumbotron>
  );
}