import React, {useState,useEffect,useContext} from 'react'
import {Form,Button} from 'react-bootstrap'

import Swal from 'sweetalert2'

import UserContext from '../userContext'

import {Redirect} from 'react-router-dom'
import '../App.css'

export default function Login(){

	const {user,setUser} = useContext(UserContext)
	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")

	const [isActive,setIsActive] = useState(false)
	const [willRedirect,setWillRedirect] = useState(false)

	useEffect(()=>{
		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email,password])

	function loginUser(e){
		e.preventDefault()
		fetch('https://thawing-refuge-32176.herokuapp.com/api/users/login',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Login failed.",
					text: data.message
				})
			} else {
				console.log(data)
				localStorage.setItem('token',data.accessToken)
				fetch('https://thawing-refuge-32176.herokuapp.com/api/users',{
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					localStorage.setItem('email',data.email)
					setUser({
						email: data.email,
					})
					setWillRedirect(true)

					Swal.fire({
						icon: "success",
						title: "Successful Log In!",
						text: `Thank you for logging in, ${data.firstName}`
					})
				})
			}
		})
		setEmail("")
		setPassword("")
	
	}

	return (

			user.email || willRedirect
			?
			<Redirect to='/'/>
			:
			<Form className="login gray" onSubmit={e => loginUser(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label>
						<b>Email</b>
					</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={(e)=>setEmail(e.target.value)} required />
				</Form.Group>
				<Form.Group controlId="userPassword">
					<Form.Label>
						<b>Password</b>
					</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password} onChange={(e)=>setPassword(e.target.value)}required />
				</Form.Group>
				{

					isActive
					?
					<Button variant="primary" type="submit">Log In!</Button>
					:
					<Button variant="primary" disabled>Log In!</Button>

				}
			</Form>
		)
}