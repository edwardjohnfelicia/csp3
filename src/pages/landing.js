import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container'
import { useHistory } from 'react-router-dom';
  
import React from 'react';

import '../App.css'
  
  
export default function JumbotronExample() {

	const history = useHistory()

	const toRegister = () =>{ 
	  let path = `/register`; 
	  history.push(path);
	}

  return (
    <>
    <Jumbotron fluid className="landing">
      <Container>
        <h2>Welcome to Budgetarian!</h2>
        <p>
             Easily manage your finances, anytime! anywhere! 
        </p>
        <Button variant="primary" onClick={toRegister}>
         Register Now!
        </Button>
      </Container>
    </Jumbotron>
    </>
  );
}