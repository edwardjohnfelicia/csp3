import React, {useState,useEffect} from 'react'
import {Form,Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import {Redirect} from 'react-router-dom'
import {LinkContainer} from 'react-router-bootstrap'
import Container from 'react-bootstrap/Container'

import '../App.css'

export default function Register(){

	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword,setConfirmPassword] = useState("")

	const [isActive,setIsActive] = useState(false)
	const [willRedirect,setWillRedirect] = useState(false)

	useEffect(()=>{
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" &&password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[firstName,lastName,email,mobileNo,password,confirmPassword])

	function registerUser(e){
		e.preventDefault()
		// console.log("The page will no longer refresh because of submit.")
		fetch('https://thawing-refuge-32176.herokuapp.com/api/users',{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password,
				confirmPassword: confirmPassword
			})
		})
		.then(response => response.json())
		.then(data => {
			// console.log(data)
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Registration Failed.",
					text: data.message
				})
			} else {
				Swal.fire({
					icon: "success",
					title: "Registration Successful!",
					text: "Thank you for registering."
				})
				setWillRedirect(true)
			}
		})
		setFirstName("")
		setLastName("")
		setMobileNo("")
		setEmail("")
		setPassword("")
		setConfirmPassword("")
	}

	return (
		willRedirect
		?
		<Redirect to="/login" />
		:
		<>
		<Container  className="register gray">
		<LinkContainer to="/login">
		<h5 className="link"> Already Registered? Click here! </h5>
		</LinkContainer>
		<Form onSubmit={e=>registerUser(e)}>
			<Form.Group>
				<Form.Label><b>First Name:</b></Form.Label>
				<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e=>{setFirstName(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label><b>Last Name:</b></Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e=>{setLastName(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label><b>Email:</b></Form.Label>
				<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e=>{setEmail(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label><b>Mobile Number:</b></Form.Label>
				<Form.Control type="number" placeholder="Enter 11 Digit Mobile No." value={mobileNo} onChange={e=>{setMobileNo(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label><b>Password:</b></Form.Label>
				<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e=>{setPassword(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label><b>Confirm Password:</b></Form.Label>
				<Form.Control type="password" placeholder="Confirm Password" value={confirmPassword} onChange={e=>{setConfirmPassword(e.target.value)}} required/>
			</Form.Group>
			{
				isActive
				? <Button variant="primary" type="submit">Submit</Button>
				: <Button variant="primary" disabled>Submit</Button>
			}
			
		</Form>
		</Container>
		</>
		)

}
