import React, {useEffect, useState, useContext} from 'react'

import {Form, Button} from 'react-bootstrap'

import Swal from 'sweetalert2'
import '../App.css'

import UserContext from '../userContext'
import Container from 'react-bootstrap/Container'

function AddIncomeEntry(entryProps) {
    const {user} = useContext(UserContext)

    const [category, setCategory]= useState("")
    const [amount, setAmount]= useState("")
    const [type, setType]= useState("Income")
    const [allCategories, setAllCategories] = useState([])
    const [incomeCategories, setIncomeCategories] = useState([])

    const [update, setUpdate] = useState("")

    useEffect(()=>{
        fetch('https://thawing-refuge-32176.herokuapp.com/api/categories',{

        method: 'GET',   
        headers: {

                Authorization: `Bearer ${localStorage.getItem('token')}`

                    }
       }).then(res=>res.json())
        .then(data=>{
            // console.log(data)

            setAllCategories(data)


            // console.log(allCategories)

            let  categoryTemp = data

            console.log(categoryTemp)

            let incomeEntries = categoryTemp.filter(category=>{

                return category.type === "Income"
            })

            console.log(incomeEntries)

             setIncomeCategories(incomeEntries)

        })




    }, [update])    

           
             let formOptions = incomeCategories.map(option=>{

                return (

                    <option key={option._id}>{option.name}</option>

                    )
             })

            function addEntry(e){

                e.preventDefault()
                let token=localStorage.getItem('token')

                fetch('https://thawing-refuge-32176.herokuapp.com/api/entries',{

                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        category: category,
                        amount: amount * 1,
                        type: type
                    })
                 }).then(res=>res.json())
                 .then(data => {
                            
                    if(data.message){

                        Swal.fire({
                            icon:"success",
                            title:"Entry added",
                            text: data.message
                            
                        })
                    }else {
                        console.log(data)
                        Swal.fire({
                            icon: "error",
                            title: "Entry not added",
                            text: data.message
                        })
                    }
                 })
                 window.location.replace('/income')
                 setCategory("")
                 setAmount("")
             }

   
      return (
                <>
                <Container className="white">
                  <h2>Add Record</h2>
                    <div className="AddForm">
                     <Form onSubmit={e => addEntry(e)}>
                         <Form.Group controlId="category">
                         <Form.Label>Category:</Form.Label>
                         <br/>
                         
                            <Form.Control as="select" type="text" value={category} onChange={(e)=>setCategory(e.target.value)}> 
                                 <option>Choose</option>  
                                 {formOptions}
                            </Form.Control>     

                         </Form.Group>

                         <Form.Group controlId="amount">
                             <Form.Label>Amount:</Form.Label>
                             <Form.Control type="number" placeholder="0.00" value={amount} onChange={(e)=>setAmount(e.target.value)} required/> 
                         </Form.Group>

                         <Form.Group>
                            <Form.Label>Type: Income</Form.Label>
                            
                         </Form.Group>
                         <Button type="submit">Add</Button>
                     </Form>
                 </div>
                 </Container>
                 </>
             )

    
       
       
     

}

export default AddIncomeEntry