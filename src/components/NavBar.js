import React,{useContext} from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

import {NavLink, Link} from 'react-router-dom'

import UserContext from '../userContext'

import '../App.css'

export default function NavBar(){

const {user,unsetUser,setUser} = useContext(UserContext)

function logout(){
	unsetUser()
	setUser({
		email:null,
		isAdmin:null
	})
	window.location.replace('/login')
}
	
	return (


		<Navbar bg="light" expand="lg">
			{
			user.email
				? <Navbar.Brand as={Link} to="/"><b>Budgetarian</b></Navbar.Brand>
				: <Navbar.Brand as={Link} to="/"><b>Budgetarian</b></Navbar.Brand>
			}
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
				{
					user.email
					?
					<>
						<Nav.Link as={NavLink} to="/categories">Categories</Nav.Link>
						<Nav.Link as={NavLink} to="/income">Income</Nav.Link>
						<Nav.Link as={NavLink} to="/expense">Expenses</Nav.Link>
						{/*<Nav.Link as={NavLink} to="/monthly">Monthly</Nav.Link>
						<Nav.Link as={NavLink} to="/trend">Trend</Nav.Link>*/}
						<Nav.Link onClick={logout}>Logout</Nav.Link>
					</>
					:
					<>
						<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
						<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
					</>
				}
				</Nav>
			</Navbar.Collapse>
		</Navbar>


		)

}
